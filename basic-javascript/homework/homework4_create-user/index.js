// Метод объекта позволяет внитри себя разместить данные, и после их вызвать при помощи ключа

function createNewUser() {
  let name = prompt("Please, write your name");
  let surname = prompt("Please, write your surname");
  return {
    firstName: name,
    lastName: surname,
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
  };
}
let newUser = createNewUser();
console.log(newUser.getLogin());
