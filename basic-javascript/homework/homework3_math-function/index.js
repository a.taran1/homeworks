// 1. Функции необходимы чтобы выполнить часть кода несколько раз, при этом можно изменять параметры функции в каждом отдельном случае
// 2. Аргументыры используют для того чтобы внося разные параметры можно было получать другие результаты в той же самой функции, не переписывая её.
// while (true) {
let a = +prompt("Введите число а", "");
let znak = prompt("Введите действие между a и b", "");
let b = +prompt("Введите число b", "");

math(a, znak, b);
function math(a, znak, b) {
  switch (znak) {
    case "+":
      console.log(a + b);
      break;
    case "-":
      console.log(a - b);
      break;
    case "/":
      console.log(a / b);
      break;
    case "*":
      console.log(a * b);
      break;
    default:
      alert("Дай мне знак");
      break;
  }
}
